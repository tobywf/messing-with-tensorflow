import json
import string
import numpy as np

from keras.models import Sequential
from keras.layers import Dense, Flatten

WINDOW_SIZE = 5
WINDOW_LEN = 2 * WINDOW_SIZE

with open("A Tale of Two Cities by Charles Dickens.txt", 'r') as fp:
    text = fp.read()

# for one hot
chars = sorted(set(text + string.printable))
char_to_int = {c: i for i, c in enumerate(chars)}
char_len = len(chars)

with open('onehot.json', 'w') as fp:
    json.dump(char_to_int, fp)

index = text.find("’", WINDOW_SIZE)
x = []
y = []
while index > 0:
    if text[index + 1] != '\n' and text[index + 1] == ' ':
        index = text.find("’", index + 1)
        continue

    linebreak = text[index + 1] == '\n'

    if linebreak:
        window = text[index - WINDOW_SIZE:index] + " " + text[index + 2:index + 1 + WINDOW_SIZE]
    else:
        window = text[index - WINDOW_SIZE:index] + text[index + 1:index + 1 + WINDOW_SIZE]

    try:
        assert len(window) == WINDOW_LEN, '>>>{}<<<'.format(window)
        vec = [char_to_int[c] for c in window]
    except (KeyError, AssertionError):
        pass
    else:
        one_hot = np.zeros((WINDOW_LEN, char_len), dtype=np.bool)
        one_hot[np.arange(WINDOW_LEN), vec] = True

        x.append(one_hot)
        y.append(linebreak)
    finally:
        index = text.find("’", index + 1)

print('Datapoints:', len(y))

x_train = np.array(x, dtype=np.bool)
y_train = np.array(y, dtype=np.bool)

# np.savez('data.npz', x=x, y=y)

model = Sequential()
model.add(Flatten(input_shape=(WINDOW_LEN, char_len)))
model.add(Dense(char_len, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])

model.fit(x_train, y_train, epochs=10, batch_size=10)

with open('model.json', 'w') as fp:
    fp.write(model.to_json())

model.save_weights('model.h5')
