import json
import numpy as np

from keras.models import model_from_json

WINDOW_SIZE = 5
WINDOW_LEN = 2 * WINDOW_SIZE

with open('onehot.json', 'r') as fp:
    char_to_int = json.load(fp)
char_len = len(char_to_int)

with open('model.json', 'r') as fp:
    model = model_from_json(fp.read())

model.load_weights('model.h5')

with open("Alice's Adventures in Wonderland by Lewis Carroll.txt", 'r') as fp:
    original = fp.read()

text = original.replace("’\n", "’ ")

truths = []
indices = []
positions = []
index = text.find("’ ", WINDOW_SIZE)
while index > 0:
    window = text[index - WINDOW_SIZE:index] + text[index + 1:index + 1 + WINDOW_SIZE]

    try:
        assert len(window) == WINDOW_LEN, '>>>{}<<<'.format(window)
        vec = [char_to_int[c] for c in window]
    except (KeyError, AssertionError):
        pass
    else:
        truths.append(original[index + 1] == '\n')
        indices.append(index)

        one_hot = np.zeros((WINDOW_LEN, char_len), dtype=np.bool)
        one_hot[np.arange(WINDOW_LEN), vec] = True

        positions.append(one_hot)
    finally:
        index = text.find("’ ", index + 1)

length = len(truths)
predictions = model.predict(np.array(positions))
assert len(predictions) == length

for cutoff in (0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99):
    correct = 0
    for truth, prediction in zip(truths, predictions):
        linebreak = prediction > cutoff
        if linebreak == truth:
            correct += 1

    print('Cutoff: {}, tests: {}, correct: {}, incorrect: {}'.format(cutoff, length, correct, length - correct))

for index, prediction in zip(indices, predictions):
    if prediction > 0.9:
        text = text[:index + 1] + '\n' + text[index + 2:]

with open('test.txt', 'w') as fp:
    fp.write(text)
