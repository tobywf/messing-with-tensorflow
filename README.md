Messing around with Keras and TensorFlow
========================================

Trying to predict line-breaks in English text.

Books tested so far from Project Gutenberg's [top downloads](https://www.gutenberg.org/ebooks/search/?sort_order=downloads):

1. Pride and Prejudice by Jane Austen: double quotes
2. The Adventures of Tom Sawyer by Mark Twain: double quotes
3. Alice's Adventures in Wonderland by Lewis Carroll: single quotes
4. A Tale of Two Cities by Charles Dickens: double quotes

And some others:

* Kim by Rudyard Kipling: single quotes
* Ulysses by James Joyce: no quotes
